" ======================================
" Custom key mapping
" ======================================

let mapleader = "\<Space>"


" =======
" Windows
" =======
" Window vsplit
nnoremap <silent> <Leader>wv :vsplit<CR>
" Window split
nnoremap <silent> <Leader>ws :split<CR>
" Window close
nnoremap <silent> <Leader>wc :q<CR>
" Window left
nnoremap <silent> <Leader>wh <C-w>h
" Window down
nnoremap <silent> <Leader>wj <C-w>j
" Window up
nnoremap <silent> <Leader>wk <C-w>k
" Window right
nnoremap <silent> <Leader>wl <C-w>l
" Window move left
nnoremap <silent> <Leader>wmh <C-w>H
" Window move down
nnoremap <silent> <Leader>wmj <C-w>J
" Window move up
nnoremap <silent> <Leader>wmk <C-w>K
" Window move right
nnoremap <silent> <Leader>wml <C-w>L
" Window close others
nnoremap <silent> <Leader>wo <C-w>o


" =======
" Buffers
" =======
" Next buffer
nnoremap <silent> <Tab> :bnext<CR>
nnoremap <silent> <Leader>bn :bnext<CR>
nnoremap <silent> <Leader>b] :bnext<CR>
" Previous buffer
nnoremap <silent> <S-Tab> :bprev<CR>
nnoremap <silent> <Leader>bp :bprev<CR>
nnoremap <silent> <Leader>b[ :bprev<CR>
" List buffers
nnoremap <silent> <Leader>bl :Telescope buffers<CR>
" Kill buffer
nnoremap <silent> <Leader>bk :bdelete<CR>
nnoremap <silent> <Leader>bK :bdelete!<CR>
" Save buffer
nnoremap <silent> <Leader>bs :w<CR>


" ========
" Vim Plug
" ========
nnoremap <silent> <Leader>pu :PlugUpdate<CR>


" ==============
" Fugitive (Git)
" ==============
nnoremap <silent> <Leader>gg :Gstatus<CR>
nnoremap <silent> <Leader>gc :Git commit<CR>
nnoremap <silent> <Leader>gd :Gvdiff<CR>
nnoremap <silent> <Leader>ga :Gwrite<CR>
nnoremap <silent> <Leader>gb :Gblame<CR>
nnoremap <silent> <Leader>gf :Git fetch<CR>
nnoremap <silent> <Leader>gp :Git push<CR>
nnoremap <silent> <Leader>gP :Git pull<CR>


" =========
" Tree
" =========
nnoremap <silent> <Leader>tt :NvimTreeToggle<CR>
nnoremap <silent> <Leader>tf :NvimTreeFindFile<CR>
nnoremap <silent> <Leader>tc :NvimTreeClose<CR>


" ===========
" Indentation
" ===========
vnoremap <silent> < <gv
vnoremap <silent> > >gv


" =========
" Move code
" =========
vnoremap <silent> J :m '>+1<CR>gv=gv
vnoremap <silent> K :m '<-2<CR>gv=gv


" ======
" Search
" ======
" nnoremap <silent> <Leader><Esc> :noh<CR>


" ==========
" Completion
" ==========
inoremap <expr> <Esc> pumvisible() ? "\<C-e>" : "\<Esc>"
inoremap <expr> <C-j> pumvisible() ? "\<C-n>" : "\<C-j>"
inoremap <expr> <C-k> pumvisible() ? "\<C-p>" : "\<C-k>"
inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<CR>"


" =========
" Telescope
" =========
nnoremap <silent> <C-p> :Telescope find_files<CR>
