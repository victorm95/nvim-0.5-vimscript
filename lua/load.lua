local treesitter_config = require('nvim-treesitter.configs')
local lsp_config = require('lspconfig')

-- {{{ Treeitter
treesitter_config.setup {
  highlight = {
    ensure_installed = "maintained",
    enable = {
      "go",
      "javascript",
      "typescript",
      "lua",
      "html",
      "vue",
      "css",
      "fennel",
      "json",
      "jsdoc",
      "python",
      "php"
    }
  }
}
-- }}}


-- {{{ LSP
-- lsp_config.gopls.setup {}

-- lsp_config.tsserver.setup {}

-- lsp_config.vuels.setup {}

lsp_config.efm.setup {
  filetypes = {"javascript", "javascriptreact", "javascript.jsx", "typescript", "typescriptreact", "typescript.tsx", "json", "html", "css"},
  on_attach = function(client, bufnr)
    local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
    local opts = { noremap=true, silent=true }

    if client.resolved_capabilities.document_formatting then
      buf_set_keymap("n", "<Leader>f", "<cmd>lua vim.lsp.buf.formatting()<CR>", opts)
      vim.api.nvim_exec([[
        augroup format_on_save
          autocmd!
          autocmd BufWritePre * lua vim.lsp.buf.formatting()
        augroup END
      ]], false)
    end
  end,
  init_options = {
    documentFormatting = true
  },
  settings = {
    rootMarkers = {".git/"},
    languages = {
      javascript = {
        {lintCommand = "./node_modules/.bin/eslint --format=unix --stdin", lintStdin = true, lintIgnoreExitCode = true},
        {formatCommand = "./node_modules/.bin/prettier"}
      },
      json = {
        {formatCommand = "./node_modules/.bin/prettier --parser json"}
      },
      html = {
        {formatCommand = "./node_modules/.bin/prettier --parser html"}
      },
      css = {
        {formatCommand = "./node_modules/.bin/prettier --parser css"}
      }
    }
  }
}
-- }}}

-- {{{ Autocomplete
local on_attach = function(client, bufnr)
  local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
  local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end

  buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')

  -- Mappings.
  local opts = { noremap=true, silent=true }
  buf_set_keymap('n', 'gD', '<Cmd>lua vim.lsp.buf.declaration()<CR>', opts)
  buf_set_keymap('n', 'gd', '<Cmd>lua vim.lsp.buf.definition()<CR>', opts)
  buf_set_keymap('n', 'K', '<Cmd>lua vim.lsp.buf.hover()<CR>', opts)
  buf_set_keymap('n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
  buf_set_keymap('n', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
  -- buf_set_keymap('n', '<Leader>wa', '<cmd>lua vim.lsp.buf.add_workLeader_folder()<CR>', opts)
  -- buf_set_keymap('n', '<Leader>wr', '<cmd>lua vim.lsp.buf.remove_workLeader_folder()<CR>', opts)
  -- buf_set_keymap('n', '<Leader>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workLeader_folders()))<CR>', opts)
  buf_set_keymap('n', '<Leader>D', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
  buf_set_keymap('n', '<Leader>rn', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
  buf_set_keymap('n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
  buf_set_keymap('n', '<Leader>e', '<cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<CR>', opts)
  buf_set_keymap('n', '[d', '<cmd>lua vim.lsp.diagnostic.goto_prev()<CR>', opts)
  buf_set_keymap('n', ']d', '<cmd>lua vim.lsp.diagnostic.goto_next()<CR>', opts)
  buf_set_keymap('n', '<Leader>q', '<cmd>lua vim.lsp.diagnostic.set_loclist()<CR>', opts)

  -- Set some keybinds conditional on server capabilities
  if client.resolved_capabilities.document_formatting then
    buf_set_keymap("n", "<Leader>f", "<cmd>lua vim.lsp.buf.formatting()<CR>", opts)
    vim.api.nvim_exec([[
      augroup format_on_save
        autocmd!
        autocmd BufWritePre * lua vim.lsp.buf.formatting()
      augroup END
    ]], false)
  elseif client.resolved_capabilities.document_range_formatting then
    buf_set_keymap("n", "<Leader>f", "<cmd>lua vim.lsp.buf.range_formatting()<CR>", opts)
    vim.api.nvim_exec([[
      augroup range_format_on_save
        autocmd!
        autocmd BufWritePre * lua vim.lsp.buf.range_formatting()
      augroup END
    ]], false)
  end

  -- Set autocommands conditional on server_capabilities
  if client.resolved_capabilities.document_highlight then
    vim.api.nvim_exec([[
      hi LspReferenceRead cterm=bold ctermbg=red guibg=LightYellow
      hi LspReferenceText cterm=bold ctermbg=red guibg=LightYellow
      hi LspReferenceWrite cterm=bold ctermbg=red guibg=LightYellow
    ]], false)
  end
end

-- Use a loop to conveniently both setup defined servers
-- and map buffer local keybindings when the language server attaches
local servers = {"gopls", "tsserver", "vuels"}
for _, lsp in ipairs(servers) do
  lsp_config[lsp].setup { on_attach = on_attach }
end
-- }}}
