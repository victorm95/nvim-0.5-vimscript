
" =============
" Color schemes
" =============
Plug 'dracula/vim'
Plug 'arcticicestudio/nord-vim'
Plug 'joshdick/onedark.vim'
Plug 'morhetz/gruvbox'


" =========
" Languages
" =========
" Plug 'fatih/vim-go', { 'do': ':GoInstallBinaries' }
" Plug 'elzr/vim-json'
Plug 'tpope/vim-git'
" Plug 'othree/html5.vim'
Plug 'pangloss/vim-javascript'
Plug 'stephpy/vim-yaml', { 'for': 'yaml' }
Plug 'dag/vim-fish', { 'for': 'fish' }
" typescript
" Plug 'HerringtonDarkholme/yats.vim'
" Plug 'posva/vim-vue', { 'on': 'vue' }


" =========
" Utilities
" =========
Plug 'tpope/vim-surround'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-repeat'
Plug 'vim-airline/vim-airline'
" Plug 'scrooloose/nerdtree'
" Plug 'ctrlpvim/ctrlp.vim'
Plug 'airblade/vim-gitgutter'
" Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'raimondi/delimitmate'
" Plug 'yggdroot/indentline'
" Plug 'dense-analysis/ale'
" Plug 'ervandew/supertab'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'neovim/nvim-lspconfig'
Plug 'kyazdani42/nvim-web-devicons'
Plug 'kyazdani42/nvim-tree.lua'
" {{ Telescope
Plug 'nvim-lua/popup.nvim'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'
" }}
